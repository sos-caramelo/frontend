// src/middleware/auth-guard.js

const AuthGuard = (to, from, next) => {
  // Verifique se o usuário está autenticado (por exemplo, verifique a existência do token JWT)
  const token = localStorage.getItem('jwtToken')

  if (token) {
    // O usuário está autenticado, permita o acesso à rota protegida
    next()
  } else {
    // O usuário não está autenticado, redirecione para a página de login
    window.alert('Impossivel Acessar a Pagina Faça o Login')
    next({ path: '/admin/login' })
  }
}

export default AuthGuard
