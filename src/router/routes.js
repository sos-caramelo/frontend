import AuthGuard from 'src/middleware/auth-guard'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'inicio', component: () => import('pages/PaginaInicial.vue') },
      { path: 'AjudeNossaCausa', name: 'AjudeNossaCausa', component: () => import('pages/AjudeNossaCausa.vue') },
      { path: 'Conscientize', name: 'Conscientize', component: () => import('pages/ConscientizeSe.vue') },
      { path: 'NovoChamado', name: 'NovoChamado', component: () => import('pages/NovoChamado.vue') },
      { path: 'Mapa', name: 'Mapa', component: () => import('pages/LMapPage.vue') },
      { path: 'Adocao', name: 'Adocao', component: () => import('pages/AdocaoResponsavel.vue') },
      { path: 'Fale-Conosco', name: 'FaleConosco', component: () => import('pages/FaleConosco.vue') },
      { path: 'Denuncie', name: 'Denuncie', component: () => import('pages/DenuncieNaoSeCale.vue') },
      { path: 'SobreNos', name: 'SobreNos', component: () => import('pages/OQueFazemos.vue') },
      { path: 'Telefone-endereco', name: 'Telefone-endereco', component: () => import('pages/TelefoneEndereco.vue') }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: 'login', component: () => import('pages/admin/LoginAdmin.vue') } // Rota para a página de login
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      { path: 'dashboard', name: 'dashboard', component: () => import('pages/admin/IndexPage.vue'), beforeEnter: AuthGuard },
      { path: 'tiposanimais', name: 'tiposanimais', component: () => import('pages/admin/TiposAnimais.vue'), beforeEnter: AuthGuard },
      { path: 'tiposcategorias', name: 'tiposcategorias', component: () => import('pages/admin/TiposCategoriasAnimais.vue'), beforeEnter: AuthGuard },
      { path: 'idadeanimais', name: 'idadeanimais', component: () => import('pages/admin/IdadeAnimais.vue'), beforeEnter: AuthGuard }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
